import React from 'react';
import Header from '../Header/Header';
import Purchase from '../Purchase/Purchase';

const Content = () => {
    return (
        <main>
            <Header />
            <Purchase />
        </main>
    );
};


export default Content;
