import produce from 'immer';
import React, { useCallback, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { FormattedMessage, useIntl } from 'react-intl';
import Button from '../../components/Button';
import Card from '../../components/Card';
import Img from '../../components/Img';
import Table from '../../components/Table';
import { useGetVatRates, useGetExchangeRates } from './hooks';
import messages from './messages';
import { useLocale } from '../../i18n/hooks';

const Purchase = () => {
    const [cart, setCart] = useState([]);
    const [net, setNet] = useState(0);
    const [vat, setVat] = useState(0);
    const [total, setTotal] = useState(0);
    const [exchangeConstants] = useState({ de: 'EUR', pl: 'PLN' });

    const tableInit = [
        {
            heading: <FormattedMessage {...messages.product} />,
            lookFor: 'name',
        },
        {
            heading: <FormattedMessage {...messages.quantity} />,
            lookFor: 'quantity',
        },
        {
            heading: <FormattedMessage {...messages.price} />,
            lookFor: 'price',
            valueManipulator(price) {
                return `${getExchangePrice(price)}${intl.formatMessage({
                    ...messages.currencySymbol,
                })}`;
            },
        },
    ];

    const [products, setProducts] = useState([
        {
            id: 1,
            name: 'iPhone 11',
            picture:
                'https://files.refurbed.com/ii/iphone-11-pro-1619179577.jpg',
            price: 399,
            stock: 3,
        },
        {
            id: 2,
            name: 'Samsung Galaxy S8',
            picture:
                'https://files.refurbed.com/ii/64-gb-schwarz-single-sim-1562659918.jpg',
            price: 275,
            stock: 5,
        },
    ]);

    const [vatRates] = useGetVatRates();
    const [exchangeRates] = useGetExchangeRates();
    const [locale] = useLocale();
    const intl = useIntl();

    const updateProductStock = (index) => {
        const nextState = produce(products, (draft) => {
            draft[index].stock--;
        });
        setProducts(nextState);
    };

    const getExchangePrice = useCallback(
        (eurPrice) => {
            if (locale === 'de') return eurPrice;
            else if (exchangeRates && exchangeRates.rates) {
                let exchangeRate =
                    parseFloat(eurPrice) *
                    parseFloat(exchangeRates.rates[exchangeConstants[locale]]);

                return exchangeRate.toFixed(2);
            }
        },
        [exchangeConstants, exchangeRates, locale]
    );

    const addToCart = (index) => {
        let product = products[index];
        //Check if the same product already in the cart
        let idx = cart.findIndex((item) => item.id === product.id);

        if (idx !== -1) {
            const nextState = produce(cart, (draft) => {
                draft[idx].quantity++;
            });
            setCart(nextState);
        } else {
            setCart([...cart, { ...product, quantity: 1 }]);
        }
        updateProductStock(index);
    };

    const updateNet = useCallback((products) => {
        let netAmount = 0;
        products.forEach((product) => {
            // product price may be in decimals, so using parseFloat
            netAmount += parseFloat(product.price) * parseInt(product.quantity);
        });
        setNet(netAmount);
    }, []);

    const updateVat = useCallback(() => {
        const { rates } = vatRates;
        let vatAmount = 0;
        if (rates) {
            let localeVat = rates[locale.toUpperCase()].standard_rate;
            vatAmount = (parseFloat(localeVat) * net) / 100;
        }
        setVat(vatAmount);
    }, [locale, net, vatRates]);

    useEffect(() => {
        updateNet(cart);
    }, [cart, updateNet]);

    useEffect(() => {
        updateVat();
    }, [net, updateVat]);

    useEffect(() => {
        setTotal((parseFloat(net) + parseFloat(vat)).toFixed(2));
    }, [net, vat]);

    return (
        <div>
            <div
                className={`grid ${
                    products.length > 3
                        ? 'md:grid-cols-3'
                        : 'md:grid-flow-col md:auto-cols-auto'
                }  xs:grid-cols-1 gap-4 p-4 mb-4`}
            >
                {products.map((product, index) => (
                    <Card key={uuidv4()} _className='flex flex-col'>
                        <Img
                            _className='w-32 flex-grow'
                            _src={product.picture}
                            _alt='product'
                        />
                        <h3 className='text-xl mb-2'>{product.name}</h3>
                        <p className='mb-2 flex-none'>
                            {getExchangePrice(product.price)}
                            <FormattedMessage {...messages.currencySymbol} />
                        </p>
                        <Button
                            _className='sm:ml-auto sm:max-w-max flex-none'
                            _func={() => addToCart(index)}
                            _disabled={product.stock <= 0}
                            _text={intl.formatMessage({
                                ...messages.addToCart,
                            })}
                        />
                    </Card>
                ))}
            </div>

            <Card _className='md:w-1/2'>
                <h2 className='text-2xl mb-2'>
                    <FormattedMessage {...messages.cart} />
                </h2>
                <hr />

                <Table _tableInit={tableInit} _data={cart} />

                <hr />
                <p className='m-2'>
                    <FormattedMessage {...messages.net} />:{' '}
                    {getExchangePrice(net)}{' '}
                    <FormattedMessage {...messages.currencySymbol} />
                </p>
                <p className='m-2'>
                    <FormattedMessage {...messages.vat} />:{' '}
                    {getExchangePrice(vat)}{' '}
                    <FormattedMessage {...messages.currencySymbol} />
                </p>
                <p className='m-2 font-bold'>
                    <FormattedMessage {...messages.total} />:
                    {getExchangePrice(total)}{' '}
                    <FormattedMessage {...messages.currencySymbol} />
                </p>
            </Card>
        </div>
    );
};

export default Purchase;
