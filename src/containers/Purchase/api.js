import axios from 'axios';
import {
    fetchVatRatesFailure,
    fetchVatRatesRequest,
    fetchVatRatesSuccess,
    fetchExchangeRatesFailure,
    fetchExchangeRatesRequest,
    fetchExchangeRatesSuccess,
} from './actions';

const baseApiUrl = 'https://api.exchangerate.host';

export const fetchVatRatesApi = () => {
    return (dispatch) => {
        var requestURL = `${baseApiUrl}/vat_rates`;

        //Notify that request has started
        dispatch(fetchVatRatesRequest());

        axios
            .get(requestURL, {
                responseType: 'json',
            })
            .then((res) => {
                dispatch(fetchVatRatesSuccess(res.data));
            })
            .catch((err) => {
                dispatch(fetchVatRatesFailure(err));
            });
    };
};

export const fetchExchangeRatesApi = () => {
    return (dispatch) => {
        var requestURL = `${baseApiUrl}/latest`;
        //Notify that request has started
        dispatch(fetchExchangeRatesRequest());

        axios
            .get(requestURL, {
                responseType: 'json',
            })
            .then((res) => {
                dispatch(fetchExchangeRatesSuccess(res.data));
            })
            .catch((err) => {
                dispatch(fetchExchangeRatesFailure(err));
            });
    };
};
