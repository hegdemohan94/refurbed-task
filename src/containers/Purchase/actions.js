import {
    FETCH_EXCHANGE_RATES,
    FETCH_EXCHANGE_RATES_FAILURE,
    FETCH_EXCHANGE_RATES_SUCCESS,
    FETCH_VAT_RATES,
    FETCH_VAT_RATES_FAILURE,
    FETCH_VAT_RATES_SUCCESS,
} from './constants';

/**Vat rate actions */
export const fetchVatRatesRequest = (data) => ({
    type: FETCH_VAT_RATES,
    payload: data,
});

export const fetchVatRatesSuccess = (data) => ({
    type: FETCH_VAT_RATES_SUCCESS,
    payload: data,
});
export const fetchVatRatesFailure = (error) => ({
    type: FETCH_VAT_RATES_FAILURE,
    payload: {},
    error: error,
});

/**Exchange rate actions */
export const fetchExchangeRatesRequest = (data) => ({
    type: FETCH_EXCHANGE_RATES,
    payload: data,
});

export const fetchExchangeRatesSuccess = (data) => ({
    type: FETCH_EXCHANGE_RATES_SUCCESS,
    payload: data,
});
export const fetchExchangeRatesFailure = (error) => ({
    type: FETCH_EXCHANGE_RATES_FAILURE,
    payload: {},
    error: error,
});
