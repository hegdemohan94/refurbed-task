import produce from 'immer';
import {
    CLEAN_EXCHANGE_RATES,
    CLEAN_VAT_RATES,
    DEFAULT_EXCHANGE_RATES,
    DEFAULT_VAT_RATES,
    FETCH_EXCHANGE_RATES,
    FETCH_EXCHANGE_RATES_FAILURE,
    FETCH_EXCHANGE_RATES_SUCCESS,
    FETCH_VAT_RATES,
    FETCH_VAT_RATES_FAILURE,
    FETCH_VAT_RATES_SUCCESS,
} from './constants';

export const vatRatesReducer = (
    state = DEFAULT_VAT_RATES,
    { type, payload = {}, error = null }
) =>
    produce(state, (draft) => {
        switch (type) {
            case CLEAN_VAT_RATES:
                return { ...DEFAULT_VAT_RATES };
            case FETCH_VAT_RATES:
                draft.loading = true;
                break;
            case FETCH_VAT_RATES_SUCCESS:
                draft.loading = false;
                draft.data = payload;
                draft.error = error;
                break;
            case FETCH_VAT_RATES_FAILURE:
                draft.loading = false;
                draft.data = payload;
                draft.error = error;
                break;
            default:
                return draft;
        }
    });

export const exchangeRatesReducer = (
    state = DEFAULT_EXCHANGE_RATES,
    { type, payload = {}, error = null }
) =>
    produce(state, (draft) => {
        switch (type) {
            case CLEAN_EXCHANGE_RATES:
                return { ...DEFAULT_EXCHANGE_RATES };
            case FETCH_EXCHANGE_RATES:
                draft.loading = true;
                break;
            case FETCH_EXCHANGE_RATES_SUCCESS:
                draft.loading = false;
                draft.data = payload;
                draft.error = error;
                break;
            case FETCH_EXCHANGE_RATES_FAILURE:
                draft.loading = false;
                draft.data = payload;
                draft.error = error;
                break;
            default:
                return draft;
        }
    });
