import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { fetchExchangeRatesApi, fetchVatRatesApi } from './api';
import { CLEAN_EXCHANGE_RATES, CLEAN_VAT_RATES } from './constants';

export const useGetVatRates = (fetch = true) => {
    const [vatRates, setVatRates] = useState({});
    const vatRatesFromState = useSelector((state) => state.vatRates);
    const { data, loading } = vatRatesFromState;
    const dispatch = useDispatch();

    useEffect(() => {
        if (fetch) {
            dispatch(fetchVatRatesApi());
        }
    }, [fetch, dispatch]);

    useEffect(() => {
        if (data) {
            setVatRates(data);
        }
    }, [data]);

    useEffect(() => {
        return function cleanup() {
            dispatch({ type: CLEAN_VAT_RATES });
        };
    }, [dispatch]);

    return [vatRates, loading];
};

export const useGetExchangeRates = (fetch = true) => {
    const [exchangeRates, setExchangeRates] = useState({});
    const exchangeRatesFromState = useSelector((state) => state.exchangeRates);
    const { data, loading } = exchangeRatesFromState;
    const dispatch = useDispatch();

    useEffect(() => {
        if (fetch) {
            dispatch(fetchExchangeRatesApi());
        }
    }, [fetch, dispatch]);

    useEffect(() => {
        if (data) {
            setExchangeRates(data);
        }
    }, [data]);

    useEffect(() => {
        return function cleanup() {
            dispatch({ type: CLEAN_EXCHANGE_RATES });
        };
    }, [dispatch]);

    return [exchangeRates, loading];
};
