/*
 * Purchase - Messages
 *
 * This contains all the text for the Purchase container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'containers.Purchase';

export default defineMessages({
    currencySymbol: {
        id: `${scope}.currencySymbol`,
        defaultMessage: '',
    },
    cart: {
        id: `${scope}.cart`,
        defaultMessage: 'Cart',
    },
    product: {
        id: `${scope}.product`,
        defaultMessage: 'Product',
    },
    quantity: {
        id: `${scope}.quantity`,
        defaultMessage: 'Quantity',
    },
    price: {
        id: `${scope}.price`,
        defaultMessage: 'Price',
    },
    net: {
        id: `${scope}.net`,
        defaultMessage: 'Net',
    },
    vat: {
        id: `${scope}.vat`,
        defaultMessage: 'VAT',
    },
    total: {
        id: `${scope}.total`,
        defaultMessage: 'Total',
    },
    addToCart: {
        id: `${scope}.addToCart`,
        defaultMessage: 'Add to cart',
    },
});
