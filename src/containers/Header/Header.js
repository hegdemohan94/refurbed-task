import React, { useEffect, useState } from 'react';
import Img from '../../components/Img';
import Select from '../../components/Select';
import { useLocale } from '../../i18n/hooks';
import { getAvailableLocale } from '../../i18n/utils';
import Logo from '../../Icons/Logo';
const Header = () => {
    const [locale, setLocale] = useLocale();
    const [currentLocale, setCurrentLocale] = useState(locale);
    const options = getAvailableLocale();

    useEffect(() => {
        setLocale(currentLocale);
    }, [currentLocale, setLocale]);

    const handleLocaleChange = (e) => {
        setCurrentLocale(e.target.value);
    };

    return (
        <div>
            <header className='bg-blue-500 p-4'>
                <Select
                    _value={currentLocale}
                    _options={options}
                    _handleChange={handleLocaleChange}
                />
                <Img _alt='company' _className='w-32' _src={Logo} />
            </header>
        </div>
    );
};

export default Header;
