import { lazy } from 'react';

const Content = lazy(() => import('./containers/Content/Content.js'));

const routes = [{ path: '/', element: Content }];

export default routes;
