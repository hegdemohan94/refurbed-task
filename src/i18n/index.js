import React from 'react';
import PropTypes from 'prop-types';
import { IntlProvider } from 'react-intl';
import flatten from 'flat';

export const LanguageProvider = (props) => {
    const { locale, messages, children } = props;

    return (
        <IntlProvider locale={locale} messages={flatten(messages)}>
            {React.Children.only(children)}
        </IntlProvider>
    );
};

LanguageProvider.propTypes = {
    locale: PropTypes.string,
    messages: PropTypes.object,
    children: PropTypes.element.isRequired,
};

export default LanguageProvider;
