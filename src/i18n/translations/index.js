import de from './de.json';
import pl from './pl.json';

const translations = {
    de,
    pl,
};
export default translations;
