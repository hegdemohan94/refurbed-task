import { useDispatch, useSelector } from 'react-redux';
import { setLocaleRequest } from './actions';

export const useLocale = () => {
    const locale = useSelector((state) => state.locale);
    const { lang } = locale;
    const dispatch = useDispatch();

    const setLocale = (selectedLanguage) => {
        dispatch(setLocaleRequest(selectedLanguage));
    };

    return [lang, setLocale];
};
