import { SET_LOCALE } from './constants';

/**Locale actions */
export const setLocaleRequest = (data) => ({
    type: SET_LOCALE,
    payload: data,
});
