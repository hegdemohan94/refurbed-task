export const getAvailableLocale = () => {
    return [
        {
            name: 'DE (EUR)',
            val: 'de',
        },
        {
            name: 'PL (PLN)',
            val: 'pl',
        },
    ];
};
