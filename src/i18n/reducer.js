import produce from 'immer';
import { DEFAULT_LOCALE, SET_LOCALE } from './constants';

export const localeReducer = (state = DEFAULT_LOCALE, { type, payload = {} }) =>
    produce(state, (draft) => {
        switch (type) {
            case SET_LOCALE:
                draft.lang = payload;
                break;
            default:
                return draft;
        }
    });
