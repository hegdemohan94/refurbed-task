import { Suspense } from 'react';
import { Route, Routes } from 'react-router';
import routes from './routes';
import { v4 as uuidv4 } from 'uuid';
import LanguageProvider from './i18n';
import translations from './i18n/translations/index';
import { useLocale } from './i18n/hooks';
import { BrowserRouter } from 'react-router-dom';

const App = () => {
    const [locale] = useLocale();

    return (
        <LanguageProvider locale={locale} messages={translations[locale]}>
            <BrowserRouter>
                <Routes>
                    {routes.map((route) => {
                        return (
                            <Route
                                key={uuidv4()}
                                path={route.path}
                                element={
                                    <Suspense fallback={<div>Loading...</div>}>
                                        <route.element />
                                    </Suspense>
                                }
                            />
                        );
                    })}
                </Routes>
            </BrowserRouter>
        </LanguageProvider>
    );
};

export default App;
