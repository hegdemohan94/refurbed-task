/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';

import {
    vatRatesReducer,
    exchangeRatesReducer,
} from './containers/Purchase/reducer';
import { localeReducer } from './i18n/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default combineReducers({
    vatRates: vatRatesReducer,
    exchangeRates: exchangeRatesReducer,
    locale: localeReducer,
});
