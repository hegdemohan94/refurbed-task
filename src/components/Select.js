import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

const Select = (props) => {
    const { _value, _options = [], _handleChange = () => {} } = props;
    return (
        <select
            className='bg-white text-xs border border-gray-200 rounded p-2 float-right'
            onChange={_handleChange}
            value={_value}
        >
            {_options.map((option) => {
                return (
                    <option key={uuidv4()} value={option.val}>
                        {option.name}
                    </option>
                );
            })}
        </select>
    );
};

Select.propTypes = {
    _value: PropTypes.string,
    _options: PropTypes.array,
    _handleChange: PropTypes.func,
};

export default Select;
