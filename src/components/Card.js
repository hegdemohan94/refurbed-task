import React from 'react';
import PropTypes from 'prop-types';

const Card = (props) => {
    const { children, _className = '' } = props;
    return (
        <div className={`bg-white rounded-xl p-4 shadow-md ${_className}`}>
            {children}
        </div>
    );
};

Card.propTypes = {
    _className: PropTypes.string,
};

export default Card;
