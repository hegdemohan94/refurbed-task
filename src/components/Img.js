import React from 'react';
import PropTypes from 'prop-types';

const Img = (props) => {
    const { _src = '', _alt = '', _className = '' } = props;
    return <img className={_className} src={_src} alt={_alt} />;
};
Img.propTypes = {
    _src: PropTypes.string,
    _alt: PropTypes.string,
    _className: PropTypes.string,
};

export default Img;
