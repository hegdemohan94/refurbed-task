import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

const Table = (props) => {
    const { _tableInit = [], _data = [], _className = '' } = props;
    return (
        <table className={`table-fixed ${_className}`}>
            <thead>
                <tr>
                    {_tableInit.map((o) => {
                        return (
                            <th key={uuidv4()} className='w-1/2 text-left p-2'>
                                {o.heading}
                            </th>
                        );
                    })}
                </tr>
            </thead>
            <tbody>
                {_data.map((dataObject) => (
                    <tr key={uuidv4()}>
                        {_tableInit.map((tableInitObject) => {
                            const { lookFor, valueManipulator } =
                                tableInitObject;

                            return (
                                <td key={uuidv4()} className='p-2'>
                                    {valueManipulator
                                        ? valueManipulator(dataObject[lookFor])
                                        : dataObject[lookFor]}
                                </td>
                            );
                        })}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

Table.propTypes = {
    _tableInit: PropTypes.array,
    _data: PropTypes.array,
    _className: PropTypes.string,
};

export default Table;
