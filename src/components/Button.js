import React from 'react';
import PropTypes from 'prop-types';
const Button = (props) => {
    const { _text, _className, _func = () => {}, _disabled = false } = props;
    return (
        <button
            className={`py-2
                px-4
                bg-blue-500
                hover:bg-blue-700
                text-white
                rounded-lg
                shadow-md ${_className}`}
            onClick={() => _func()}
            disabled={_disabled}
        >
            {_text}
        </button>
    );
};

Button.prototype = {
    _text: PropTypes.string,
    _className: PropTypes.string,
    _func: PropTypes.func,
    _disabled: PropTypes.bool,
};

export default Button;
